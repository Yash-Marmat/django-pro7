from django.shortcuts import render # not using here
from django.views.generic import CreateView   # for adding new features
from .forms import CustomUserCreationForm  # handles usernames, passwords and age fields
from django.urls import reverse_lazy  # redirects us to a page


class SignUpView(CreateView):
    form_class    = CustomUserCreationForm
    success_url   = reverse_lazy('login')     # redirects us to the login page
    template_name = 'signup.html'