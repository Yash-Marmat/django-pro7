-in this project we are using bootstrap to make our newspaper project look better
-continution of project 6
-also making password change facilities (in registration folder i created new templates for that)
-created password change, conformation and reset page.
-for password reset i mentioned one line code (EMAIL_BACKEND) in bottom of settings.py file (alows us to use email
 password revory methods via django default sever) 

-also contains below templates:
    password_reset_form.html
    password_reset_done.html
    password_reset_confirm.html
    password_reset_complete.html

-Note: About sign Up Page

For example, if you do a search for “150 characters or fewer” you’ll find yourself on
the django/contrib/auth/models.py page located here on line 301. The text comes as
part of the auth app, on the username field for AbstractUser.

We have three options now:
• override the existing help_text
• hide the help_text
• restyle the help_text

We’ll choose the third option since it’s a good way to introduce the excellent 3rd party
package django-crispy-forms.
Working with forms is a challenge and django-crispy-forms makes it easier to write
DRY code.
First stop the local server with Control+c. Then use Pipenv to install the package in
our project.

Command Line
(news) $ pipenv install django-crispy-forms==1.8.1

Add the new app to our INSTALLED_APPS list in the settings.py file. As the number of
apps starts to grow, I find it helpful to distinguish between 3rd party apps and local
apps I’ve added myself. (shown in settings.py file)

-Note: bootstrap

Since we’re using Bootstrap4 we should also add that config to our settings.py file.
This goes on the bottom of the file.

Code
# newspaper_project/settings.py

CRISPY_TEMPLATE_PACK = 'bootstrap4'

Now in our signup.html template we can quickly use crispy forms. First we load
crispy_forms_tags at the top and then swap out {{ form.as_p }} for {{ form|crispy
}}.